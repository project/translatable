<?php

/**
 * @file
 * Provides generic object translation services.
 */

/**
 * Implementation of hook_menu().
 *
 * Due to its early execution, hook_menu() is a good place to translate
 * variables.
 */
function translatable_object_menu($may_cache) {
  static $processed = FALSE;
  $items = array();

  // Translate system variables.
  if (!$processed && !$may_cache) {
    global $conf;
    // Fetch translated variables for the current locale.
    $variables = translatable_find('translation', "object_name = 'variable' AND locale = '". translatable_get_locale() ."'");
    foreach ($variables as $variable) {
      // Check whether a variable name exists in source variables.
      if (array_key_exists($variable['object_field'], $conf)) {
        // Replace source variable with translation.
        $conf[$variable['object_field']] = $variable['translation'];
      }
    }
    $processed = TRUE;
  }
  
  // Experimental taxonomy UI.
  /*
  if (!$may_cache) {
    if (arg(2) == 'taxonomy' && arg(3) == 'edit' && arg(4) == 'term' && is_numeric(arg(5))) {
      $items[] = array(
        'path' => 'admin/content/taxonomy/edit/term/'. arg(5),
        'type' => MENU_CALLBACK,
      );
      $items[] = array(
        'path' => 'admin/content/taxonomy/edit/term/'. arg(5) .'/view',
        'title' => t('View'),
        'type' => MENU_DEFAULT_LOCAL_TASK,
        'weight' => -10,
      );
      $items[] = array(
        'path' => 'admin/content/taxonomy/edit/term/'. arg(5) .'/translations',
        'title' => t('Translations'),
        'callback' => 'translatable_node_node_page',
        'access' => user_access('access translatable'),
        'type' => MENU_LOCAL_TASK,
        'weight' => 3,
      );
      if (arg(6) == 'translation' && translatable_validate_locale(arg(7))) {
        $items[] = array(
          'path' => 'admin/content/taxonomy/edit/term/'. arg(5) .'/translation/'. arg(7),
          'title' => t('Edit translation'),
          'callback' => 'translatable_node_edit_translation',
          'callback arguments' => array('taxonomy_admin_term_edit', arg(5), arg(7)),
          'weight' => 1,
          'type' => MENU_LOCAL_TASK,
        );
      }
    }
  }
  */
  return $items;
}

/**
 * Implementation of hook_form_alter().
 */
function translatable_object_form_alter($form_id, &$form) {
  if (translatable_alter_core_forms($form_id, $form)) {
    // Enable translation language block.
    translatable_adminlocale_formselect($form_id, $form);

    $languages = translatable_available_languages();
    $adminlocale = translatable_get_adminlocale();
    if (translatable_get_default_locale() != $adminlocale) {
      drupal_set_title(t('%language translation of %title', array('%language' => $languages[$adminlocale], '%title' => drupal_get_title())));

      // Translate fields.
      $source_form = $form;
      $condition  = "object_name = '". $source_form['#translatable_object'] ."'";
      $condition .= " AND object_key = '". $source_form['#translatable_key'] ."'";
      $condition .= " AND locale = '". translatable_get_adminlocale() ."'";
      $items = translatable_find('translation', $condition);
      translatable_translate_form_values($form, $items);

      // Filter out untranslated fields.
      translatable_filter_form($form, $source_form);

      // Replace submit callback(s); submitting translations to other callbacks
      // potentially breaks everything.
      $form['#submit'] = array('translatable_object_form_submit' => array($source_form));
    }
  }
}

/**
 * Form submit callback.
 */
function translatable_object_form_submit($form_id, $form_values, $source_form) {
  $variables = translatable_get_translatable_fields($source_form);
  if ($variables) {
    $translation = array(
      'object_name' => $source_form['#translatable_object'],
      'object_key' => $source_form['#translatable_key'],
      'locale' => translatable_get_adminlocale(),
    );
    foreach ($variables as $name) {
      if (array_key_exists($name, $form_values)) {
        $translation['object_field'] = $name;
        $translation['translation'] = $form_values[$name];
        translatable_save('translation', $translation);
      }
    }
    // Reset the cached menu, if a menu item is translated.
    if ($source_form['#translatable_object'] == 'menu') {
      menu_rebuild();
    }
  }
}

/**
 * Form submit callback to delete translations.
 */
function translatable_object_form_delete($form_id, $form_values, $source_form) {
  translatable_delete_all('translation', "object_name = '". $source_form['#translatable_object'] ."' AND object_key = '". $source_form['#translatable_key'] ."'");
}

/**
 * Make certain forms in Drupal core #translatable-aware.
 *
 * Basically, any system variable is translatable.  Thus, we only need to
 * disable untranslatable ones, but we can also return only TRUE to allow
 * translation of all form fields.
 * For any $form_id that is not covered by translatable rules, we return FALSE
 * by default.
 */
function translatable_alter_core_forms($form_id, &$form) {
  // Default properties for system_settings_forms.
  $variables_properties = array(
    '#translatable_object' => 'variable',
    '#translatable_key' => $form['form_id']['#value'],
  );
  switch ($form_id) {
    // Blocks.
    case 'block_admin_configure':
      if (arg(3) == 'add') {
        return FALSE;
      }
      $form['block_settings']['title']['#translatable'] = TRUE;
      $form['block_settings']['info']['#translatable'] = TRUE;
      $form['block_settings']['body_filter']['body']['#translatable'] = TRUE;
      $form['block_settings']['body_filter']['format']['#translatable'] = TRUE;
      $form['user_vis_settings']['#translatable'] = FALSE;
      $form['role_vis_settings']['#translatable'] = FALSE;
      $form['page_vis_settings']['#translatable'] = TRUE;
      $form['#translatable_object'] = 'block';
      $form['#translatable_key'] = $form['module']['#value'] .'-'. $form['delta']['#value'] .'-'. $GLOBALS['theme'];
      return TRUE;

    case 'block_box_delete':
      // Delete translations, if an user-generated block is deleted.
      $form['#translatable_object'] = 'block';
      $form['#translatable_key'] = 'block-'. $form['bid']['#value'] .'-'. $GLOBALS['theme'];
      $form['#submit']['translatable_object_form_delete'] = array($form);
      return FALSE;

    // Filter.
    case 'filter_admin_format_form':
      if (arg(3) == 'add') {
        return FALSE;
      }
      $form['roles']['#translatable'] = FALSE;
      $form['filters']['#translatable'] = FALSE;
      $form += $variables_properties;
      return TRUE;

    // Menu.
    case 'menu_edit_menu_form':
    case 'menu_edit_item_form':
      if (arg(4) == 'add') {
        return FALSE;
      }
      global $_menu;
      $mid = $form['mid']['#value'];
      $type = $_menu['items'][$mid]['type'];
      if (!($type & (MENU_CREATED_BY_ADMIN | MENU_MODIFIED_BY_ADMIN))) {
        return FALSE;
      }
      if ($form_id == 'menu_edit_menu_form') {
        $form['title']['#translatable'] = TRUE;
      }
      else {
        $form['title']['#translatable'] = TRUE;
        $form['description']['#translatable'] = TRUE;
        $form['path']['#translatable'] = FALSE;
        $form['expanded']['#translatable'] = FALSE;
        $form['pid']['#translatable'] = FALSE;
        $form['weight']['#translatable'] = FALSE;
      }
      $form['#translatable_object'] = 'menu';
      $form['#translatable_key'] = $mid;
      return TRUE;

    case 'menu_confirm_delete_form':
      // Delete translations, if a menu [item] is deleted.
      $form['#translatable_object'] = 'menu';
      $form['#translatable_key'] = $form['mid']['#value'];
      $form['#submit']['translatable_object_form_delete'] = array($form);
      return FALSE;

    // System.
    case 'system_site_maintenance_settings':
      $form['site_offline']['#translatable'] = FALSE;
      $form += $variables_properties;
      return TRUE;

    case 'system_date_time_settings':
      $form['date_default_timezone']['#translatable'] = FALSE;
      $form['configurable_timezones']['#translatable'] = FALSE;
      $form += $variables_properties;
      return TRUE;

    case 'system_error_reporting_settings':
      $form['error_level']['#translatable'] = FALSE;
      $form['watchdog_clear']['#translatable'] = FALSE;
      $form += $variables_properties;
      return TRUE;

    case 'system_site_information_settings':
      $form += $variables_properties;
      return TRUE;

    case 'system_themes':
      $form['status']['#translatable'] = FALSE;
      $form += $variables_properties;
      return TRUE;

    case 'system_theme_settings':
      // Translation of per-theme settings not (yet) supported.
      if (arg(4) != NULL) {
        return FALSE;
      }
      $form['logo']['logo_upload']['#translatable'] = FALSE;
      $form['favicon']['favicon_upload']['#translatable'] = FALSE;
      $form += $variables_properties;
      return TRUE;

    // Taxonomy.
    case 'taxonomy_form_vocabulary':
      if (arg(3) == 'add') {
        return FALSE;
      }
      $form['nodes']['#translatable'] = FALSE;
      $form['hierarchy']['#translatable'] = FALSE;
      $form['relations']['#translatable'] = FALSE;
      $form['tags']['#translatable'] = FALSE;
      $form['multiple']['#translatable'] = FALSE;
      $form['required']['#translatable'] = FALSE;
      $form['weight']['#translatable'] = FALSE;
      $form['#translatable_object'] = 'taxonomy_vocabulary';
      $form['#translatable_key'] = $form['vid']['#value'];
      return TRUE;

    case 'taxonomy_vocabulary_confirm_delete':
      // Delete translations, if a vocabulary is deleted.
      $form['#translatable_object'] = 'taxonomy_vocabulary';
      $form['#translatable_key'] = $form['vid']['#value'];
      $form['#submit']['translatable_object_form_delete'] = array($form);
      return FALSE;

    case 'taxonomy_form_term':
      if (arg(4) == 'add') {
        return FALSE;
      }
      $form['weight']['#translatable'] = FALSE;
      $form['#translatable_object'] = 'taxonomy_term';
      $form['#translatable_key'] = $form['tid']['#value'];
      return TRUE;

    case 'taxonomy_term_confirm_delete':
      // Delete translations, if a term is deleted.
      $form['#translatable_object'] = 'taxonomy_term';
      $form['#translatable_key'] = $form['tid']['#value'];
      $form['#submit']['translatable_object_form_delete'] = array($form);
      return FALSE;

    // User.
    case 'user_admin_settings':
      $form['registration']['user_register']['#translatable'] = FALSE;
      $form['registration']['user_email_verification']['#translatable'] = FALSE;
      $form['pictures']['user_pictures']['#translatable'] = FALSE;
      $form['pictures']['user_picture_path']['#translatable'] = FALSE;
      $form['pictures']['user_picture_dimensions']['#translatable'] = FALSE;
      $form['pictures']['user_picture_file_size']['#translatable'] = FALSE;
      $form += $variables_properties;
      return TRUE;
  }
  return FALSE;
}


