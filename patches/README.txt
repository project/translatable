
For further information about how to apply these patches see:
http://drupal.org/patch/apply

In short:

2. Determine your version of Drupal core.
3. Copy corresponding patch file to your Drupal installation's root directory.
4. Open a terminal/command line in your Drupal installation's root directory.
5. Invoke the following command:

patch -b -p0 < translatable-drupal-5.#.patch


In case something goes wrong, this patch command created backups. You will
probably find them in:

modules/block/block.module.orig
modules/taxonomy/taxonomy.module.orig


