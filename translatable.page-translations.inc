<?php

/**
 * Callback for translation management page.
 */
function translatable_page_translations($op = '', $args = array()) {
  $output = '';
  $op = isset($_POST['op']) ? $_POST['op'] : $op;

  switch ($op) {
    case 'select':
      $node = node_load(arg(1));
      $output .= translatable_node_overview($node);
      $output .= translatable_node_select($node, $args[0] ? $args[0] : translatable_get_adminlocale());
      break;
    
    case t('Assign'):
      $node = node_load(arg(1));
      $output .= translatable_node_select($node, $args[0] ? $args[0] : translatable_get_adminlocale());
      break;
    
    case 'remove':
      if ($node = node_load($args[0])) {
        $translation = translatable_findbyid('node', $node->nid);
        if ($translation['tnid'] != $node->nid) {
          translatable_save('node', array('nid' => $node->nid, 'tnid' => $node->nid, 'language' => $node->language));
          drupal_set_message('The content has been removed from the translation set.');
        }
        else {
          drupal_set_message('The source content of a translation set cannot be removed.', 'error');
        }
        drupal_goto('node/'. arg(1) .'/translations');
      }

    default:
      if (!isset($node)) {
        $node = node_load(arg(1));
      }
      $output .= translatable_node_overview($node);
  }
  return $output;
}

/**
 * Hook for the overview of a node.
 * Implements the translations tab interface.
 */
function translatable_node_overview($node) {
  drupal_set_title(t('Translations of %title', array('%title' => $node->title)));
  
  $header = array(t('Language'), t('Title'), t('Status'), t('Operations'));
  $rows = array();
  $status_text = array(0 => t('Unpublished'), 1 => t('Published'), -1 => t('Not translated'));
  foreach (translatable_available_languages() as $locale => $language) {
    $options = array();
    $translation = translatable_find('node', 'tnid = '. (int)$node->tnid ." AND language = '". $locale ."'", FALSE);
    if ($translation) {
      $trnode = db_fetch_object(db_query('SELECT n.nid, n.title, n.status, t.language, t.tnid, t.any FROM {node} n INNER JOIN {node_translatable} t ON n.nid = t.nid AND n.nid = %d', $translation['nid']));
      $title = l($trnode->title, 'node/'. $trnode->nid, NULL, 'locale='. $trnode->language);
      if ($trnode->nid == $node->tnid) {
        $language = '<strong>'. check_plain($language) .'</strong> '. t('(source)');
        $options[] = l(t('Edit'), translatable_url("node/$trnode->nid/edit", $trnode->language));
      }
      else {
        $options[] = l(t('Edit'), translatable_url("node/$trnode->tnid/edit/translation/$trnode->language", $trnode->language));
        $options[] = l(t('Remove'), "node/$node->tnid/translations/remove/$trnode->nid");
      }
      if ($trnode->any) {
        $language .= ' '. t('(any language)');
      }
      $status = $trnode->status ? t('Published') : t('Unpublished');
    }
    else {
      $title = t('n/a');
      $status = t('Not translated');
      $options[] = l(t('Create'), translatable_url("node/$node->nid/edit/translation/$locale", $locale));
      $options[] = l(t('Select'), "node/$node->nid/translations/select/$locale");
    }
    $rows[] = array($language, $title, $status, implode(' | ', $options));
  }

  return theme('table', $header, $rows);
}

/**
 * Construct an absolute URL if switch by hostname is enabled.
 */
function translatable_url($path, $locale) {
  if (variable_get('translatable_switch_byhostname', FALSE)) {
    $url  = 'http://';
    $url .= variable_get('translatable_switch_hostname_'. $locale, $locale .'.'. translatable_get_domain());
    $url .= base_path();
    $url .= $path;
    return $url;
  }
  return $path;
}

function translatable_node_select($node, $locale) {
  if (!translatable_validate_locale($locale)) {
    return;
  }

  $form['node'] = array('#type' => 'value', '#value' => $node);
  
  // Only select nodes that do not belong to another translation set.
  $selectables = translatable_find('node', '', TRUE, TRUE, array('group by' => 'tnid', 'having' => array('COUNT(tnid) = 1', "language = '$locale'")));
  if ($selectables) {
    $items = array();
    $result = pager_query(db_prefix_tables("SELECT n.nid, n.title FROM {node} n WHERE n.nid IN (%s) AND n.type = '%s' ORDER BY n.title"), 40, 0, NULL, implode(', ', array_keys($selectables)), $node->type);
    while ($trnode = db_fetch_object($result)) {
      $items[$trnode->nid] = l($trnode->title, "node/$trnode->nid");
    }
  }
  if (!empty($items)) {
    $output = drupal_get_form('translatable_node_select_form', $node, $locale, $items);
  }
  else {
    $languages = translatable_available_languages();
    $output = t('No unassigned contents available in %language.', array('%language' => $languages[$locale]));
  }
  return $output;
}

function translatable_node_select_form($node, $locale, $items) {
  $languages = translatable_available_languages();
  $translations = translatable_node_get_translations($node->nid);
  $form['#submit'] = array('translatable_node_select_form_submit' => array());
  $form['tnid'] = array(
    '#type' => 'hidden',
    '#value' => $node->tnid,
  );
  $form['language'] = array(
    '#type' => 'hidden',
    '#value' => $locale,
  );
  $form['redirect'] = array(
    '#type' => 'value',
    '#value' => "node/$node->nid/translations",
  );
  $form['nodes']['nid'] = array(
    '#type' => 'radios',
    '#title' => t('Select %language translation', array('%language' => $languages[$locale])),
    '#default_value' => isset($translations[$locale]) ? $translations[$locale]->nid : '',
    '#options' => $items,
    '#description' => t('<strong>Note:</strong> This list only contains contents that do not belong to another translation set.  If a content from another translation set should be assigned to this translation set, that content has to be removed from the existing set first.'),
  );
  $form['pager'] = array('#value' => theme('pager'));
  $form['submit'] = array('#type' => 'submit', '#value' => t('Assign'));
  return $form;
}

function translatable_node_select_form_submit($form_id, $form_values) {
  if ($form_values['tnid'] && $form_values['nid']) {
    translatable_save('node', array(
      'nid' => $form_values['nid'],
      'tnid' => $form_values['tnid'],
      'language' => $form_values['language'],
    ));
    drupal_set_message(t('The translation has been assigned to this content.'));
    drupal_goto($form_values['redirect']);
  }
}

/**
 * Fetch translations of a node.
 *
 * @param $nid
 *   The id of the node.
 */
function translatable_node_get_translations($nid) {
  $nodes = array();
  $translation = translatable_findbyid('node', $nid);
  $result = db_query("SELECT n.nid, n.title, n.status, t.language FROM {node} n INNER JOIN {node_translatable} t ON n.nid = t.nid AND t.tnid = %d AND t.nid <> %d", $translation['tnid'], $nid);
  while ($node = db_fetch_object($result)) {
    $nodes[$node->language] = $node;
  }
  return $nodes;
}


