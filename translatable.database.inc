<?php

/**
 * Returns database definitions for a given object type.
 * 
 * @param string $type
 *   An object type, either 'node' or 'translation'.
 * 
 * @return array
 *   An array containing table name and key of the object, optionally including
 *   a parent key and additional table columns.
 */
function translatable_get_object_definition($type) {
  switch ($type) {
    case 'node':
      return array(
        'table' => 'node_translatable',
        'key' => 'nid',
        'parentkey' => 'tnid',
        'language' => 'language',
        'any' => 'any',
      );
    
    case 'translation':
      return array(
        'table' => 'translatable_object',
        'key' => 'tid',
        'object_name' => 'object_name',
        'object_key' => 'object_key',
        'object_field' => 'object_field',
        'translation' => 'translation',
        'locale' => 'locale',
      );
  }
}

/**
 * Find a content translation in translation index.
 * 
 * @param string $type
 *   The object type to search for.
 * @param string $conditions
 *   A SQL where clause to search for.
 * @param bool $all
 *   Whether to return one or all query result rows.
 * @param bool $force
 *   Whether to omit cached query results.
 * @param array $args
 *   Additional query arguments, supplied as keys 'fields', 'group by', and
 *   'having'.
 * 
 * @return array
 *   Either a single (see $all) or an array containing all query result(s).
 */
function translatable_find($type, $conditions = '', $all = TRUE, $force = FALSE, $args = array()) {
  static $objects = array();
  
  $cid = $type .':'. $conditions .':'. (int)$all;
  if (isset($objects[$cid]) && !$force) {
    return $objects[$cid];
  }
  
  // Get schema definition for object type.
  $object = translatable_get_object_definition($type);
  
  $fields = '*'. (isset($args['fields']) ? ', '. implode(', ', (array)$args['fields']) : '');
  $sql = 'SELECT '. $fields .' FROM {'. $object['table'] .'}';
  if ($conditions) {
    $sql .= ' WHERE '. $conditions;
  }
  if (isset($args['group by'])) {
    $sql .= ' GROUP BY '. implode(', ', (array)$args['group by']);
  }
  if (isset($args['having'])) {
    $sql .= ' HAVING '. implode(' AND ', (array)$args['having']);
  }
  $result = $all ? db_query($sql) : db_query_range($sql, 0, 1);
  
  // A single item will be directly returned, while multiple items are stored
  // as collection.
  if ($all) {
    $items = array();
    while ($item = db_fetch_array($result)) {
      $key = $item[$object['key']];
      $items[$key] = $item;
    }
    $objects[$cid] = $items;
  }
  else {
    $objects[$cid] = db_fetch_array($result);
  }

  return $objects[$cid];
}

/**
 * Fetch a single content from translation index.
 */
function translatable_findbyid($type, $id, $force = FALSE) {
  $object = translatable_get_object_definition($type);
  return translatable_find($type, $object['key'] .' = '. (int)$id, FALSE, $force);
}

/**
 * Save a content to translation index.
 */
function translatable_save($type, $item) {
  if (!is_array($item)) {
    return FALSE;
  }
  $object = translatable_get_object_definition($type);

  if ($type == 'translation' && empty($item[$object['key']])) {
    // @doc When will the item's key not be set?
    $translation = translatable_find('translation', "object_name = '". $item['object_name'] ."' AND object_key = '". $item['object_key'] ."' AND object_field = '". $item['object_field'] ."' AND locale = '". $item['locale'] ."'", FALSE);
    $item[$object['key']] = $translation[$object['key']];
  }
  
  if (translatable_findbyid($type, $item[$object['key']])) {
    return translatable_update($type, $item);
  }
  else {
    if (isset($object['parentkey']) && empty($item[$object['parentkey']])) {
      // If no source content id is given, set source content id to current
      // content id.
      $item[$object['parentkey']] = $item[$object['key']];
    }
    return translatable_insert($type, $item);
  }
}

/**
 * Insert a new content into translation index.
 */
function translatable_insert($type, $item) {
  if (!is_array($item)) {
    return FALSE;
  }
  $object = translatable_get_object_definition($type);
  
  if ($type == 'translation') {
    $item[$object['key']] = db_next_id('{translatable_object}_'. $object['key']);
  }
  
  $table = $object['table'];
  unset($object['table']);

  $columns = $types = $values = array();
  foreach ($object as $key => $column) {
    $columns[] = $column;
    if ($key == 'key' || $key == 'parentkey') {
      $types[] = '%d';
    }
    else {
      $types[] = "'%s'";
    }
    $values[] = $item[$column];
  }
  db_query('INSERT INTO {'. $table .'} ('. implode(', ', $columns) .') VALUES ('. implode(', ', $types) .')', $values);
  return $item[$object['key']];
}

/**
 * Update a content in translation index.
 */
function translatable_update($type, $item) {
  if (!is_array($item)) {
    return FALSE;
  }
  $object = translatable_get_object_definition($type);
  
  $table = $object['table'];
  unset($object['table']);
  
  $assigns = $values = array();
  foreach ($object as $key => $column) {
    if ($key == 'key') {
      $where = $column .' = '. $item[$column];
      continue;
    }
    if ($key == 'parentkey') {
      $assigns[] = $column .' = %d';
    }
    else {
      $assigns[] = $column ." = '%s'";
    }
    $values[] = $item[$column];
  }
  return db_query('UPDATE {'. $table .'} SET '. implode(', ', $assigns) .' WHERE '. $where, $values);
}

/**
 * Delete a translation reference in translation index.
 *
 * @todo If a source content is deleted, all translations of this node have to
 *       be deleted, too.
 */
function translatable_delete($type, $id) {
  if ($id) {
    $object = translatable_get_object_definition($type);
    db_query('DELETE FROM {%s} WHERE %s = %d', $object['table'], $object['key'], $id);
  }
}

/**
 * Delete all translations for a given condition.
 */
function translatable_delete_all($type, $conditions) {
  if ($conditions) {
    $object = translatable_get_object_definition($type);
    db_query('DELETE FROM {%s} WHERE '. $conditions, $object['table']);
  }
}

