<?php

/**
 * @file
 * Provides translation services for nodes.
 */

/**
 * Implementation of hook_menu().
 */
function translatable_node_menu($may_cache) {
  $items = array();
  
  if (!$may_cache) {
    if (arg(0) == 'node' && is_numeric(arg(1))) {
      $node = node_load(arg(1));
      if (translatable_content_type_enabled($node->type)) {
        // Add link to translation overview page to local tasks.
        $items[] = array(
          'path' => 'node/'. $node->nid .'/translations',
          'title' => t('Translations'),
          'callback' => 'translatable_node_node_page',
          'access' => user_access('access translatable'),
          'type' => MENU_LOCAL_TASK,
          'weight' => 3,
        );
        if (arg(2) == 'edit' && !arg(3) && $node->tnid != $node->nid) {
          // An existing destination url would override our path, pass it
          // through instead.
          $destination = isset($_REQUEST['destination']) ? 'destination='. urlencode($_REQUEST['destination']) : NULL;
          unset($_REQUEST['destination']);
          drupal_goto('node/'. $node->tnid .'/edit/translation/'. $node->language, $destination);
        }
        if (arg(3) == 'translation' && translatable_validate_locale(arg(4))) {
          $items[] = array(
            'path' => 'node/'. $node->nid .'/edit/translation/'. arg(4),
            'title' => t('Edit translation'),
            'callback' => 'translatable_node_edit_translation',
            'callback arguments' => array($node->type .'_node_form', $node, arg(4)),
            'access' => user_access('access translatable') && node_access('update', $node),
            'type' => MENU_CALLBACK,
          );
        }
      }
    }
  }
  return $items;
}

/**
 * Menu callback; presents the translation editing form, or redirects to delete confirmation.
 */
function translatable_node_edit_translation($form_id, $node, $lang) {
  if ($_POST['op'] == t('Delete')) {
    // Redirect to node/%/delete to make the tabs disappear, passing through
    // destination.
    if (isset($_REQUEST['destination'])) {
      $destination = drupal_get_destination();
      unset($_REQUEST['destination']);
    }
    // @todo This looks wrong (we'd like to delete the translation, not the source).
    drupal_goto('node/'. $node->nid .'/delete', isset($destination) ? $destination : NULL);
  }

  $languages = translatable_available_languages();
  $adminlocale = translatable_set_adminlocale($lang, FALSE);
  drupal_set_title(t('%language translation of %title', array('%language' => $languages[$adminlocale], '%title' => $node->title)));

  // Check whether we're creating a new translation or editing an existing one.
  $translation = translatable_find('node', "tnid = $node->nid AND language = '$lang'");
  if (!$translation) {
    $node->is_new = TRUE;
  }
  else {
    $node = node_load(key($translation));
  }

  return translatable_get_form($form_id, $node);
}

/**
 * Implementation of hook_nodeapi().
 */
function translatable_nodeapi(&$node, $op, $teaser, $page) {
  if (!translatable_content_type_enabled($node->type)) {
    return;
  }

  switch ($op) {
    case 'load':
      // Add translation properties to the node.
      $properties = translatable_findbyid('node', $node->nid);
      if ($properties) {
        $extra = array(
          'language' => $properties['language'],
          'tnid' => $properties['tnid'],
          'translatable_node_any' => $properties['any'],
        );
      }
      else {
        // Legacy node which hasn't been enabled for translation before.
        // Set language to the default site language.
        $extra = array(
          'language' => translatable_get_defaultcontentlocale(),
          'tnid' => $node->nid,
        );
        if (!empty($node->nid)) {
          $extra['translatable_node_any'] = 0;
        }
      }
      return $extra;
    
    case 'prepare':
      if (isset($node->language, $node->tnid)) {
        translatable_node_update_fields($node);
      }
      // If a completely new node is created, we need to assign the language.
      if (!isset($node->nid, $node->tnid, $node->language)) {
        $node->language = translatable_get_adminlocale();
      }
      break;
    
    case 'submit':
      // If tnid of the processed node is equal to the node's nid, this is a source
      // node. We then fetch and update all translations of this node instead.
      if ($node->nid && $node->nid == $node->tnid) {
        $target_nodes = translatable_find('node', "tnid = $node->nid AND nid != $node->nid");
        foreach (array_keys($target_nodes) as $nid) {
          $target_node = node_load($nid);
          translatable_node_update_fields($target_node);
          node_save($target_node);
        }
      }
      break;
      
    case 'insert':
      // For new translations: add node to the global path index, so modules
      // like pathauto are able to find a menu path for it.
      if (!empty($node->tnid)) {
        global $_menu;
        $node_default_locale = translatable_find('node', 'tnid = '. (int)$node->tnid ." AND language = '". translatable_get_default_locale() ."'");
        if ($node_default_locale) {
          $_menu['path index']['node/'. $node->nid] = $_menu['path index']['node/'. key($node_default_locale)];
        }
      }
      // If a completely new node is created, we need to assign the language.
      if (!isset($node->tnid, $node->language)) {
        $node->tnid = $node->nid;
        $node->language = translatable_get_adminlocale();
      }
      // Fall through to save translation.
    case 'update':
      if (isset($node->language)) {
        // For new or legacy source nodes: set parent nid to ourself.
        if (empty($node->tnid)) {
          $node->tnid = $node->nid;
        }
        $translatable_node = array(
          'nid' => $node->nid,
          'tnid' => $node->tnid,
          'language' => $node->language,
          'any' => (int)$node->translatable_node_any,
        );
        translatable_save('node', $translatable_node);
      }
      break;
    
    case 'delete':
      translatable_delete('node', $node->nid);
      break;
  }
}

/**
 * Implementation of hook_form_alter().
 *
 * If the content type is enabled for translation, add a checkbox to allow
 * displaying the content for all languages.
 */
function translatable_node_form_alter($form_id, &$form) {
  if (!isset($form['type']) || $form['type']['#value'] .'_node_form' != $form_id || !translatable_content_type_enabled($form['type']['#value'])) {
    return;
  }

  // If we are on node/#/edit (a source node), our translation callback has
  // not been executed, so we need to ensure that the proper translation
  // language is set.
  if (arg(3) != 'translation') {
    translatable_set_adminlocale($form['#node']->language, FALSE);
  }
  
  // Enable translation language block.
  translatable_adminlocale_formselect($form_id, $form);

  // If configured, enable any language by default for new nodes.
  if (!isset($form['#node']->translatable_node_any)) {
    $form['#node']->translatable_node_any = variable_get('translatable_default_content_any', FALSE) ? 1 : 0;
  }
  $form['translatable_node_any'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display this content in any language'),
    '#default_value' => $form['#node']->translatable_node_any,
    '#weight' => 0,
  );
  // Disable any language property if it is already set for another language.
  $anynode = translatable_find('node', 'tnid = '. (int)$form['#node']->tnid .' AND nid != '. (int)$form['#node']->nid .' AND any = 1', FALSE);
  if ($anynode) {
    $form['translatable_node_any']['#disabled'] = TRUE;
    $form['translatable_node_any']['#description'] = t('Another translation is currently displayed in any language. <a href="!any">Click here to switch to this translation.</a>', array('!any' => url('node/'. $anynode['nid'] .'/edit')));
  }

  // Remove certain fields when creating or editing a new node and we're not
  // in the default locale.  Otherwise, contents of these fields would be
  // stored in the default locale while the node is stored in the current
  // admin locale.
  // @todo Taxonomy free tagging
  if (translatable_get_default_locale() != translatable_get_adminlocale()) {
    $form['menu']['#access'] = FALSE;
  }

  // Add parent node id, needed in nodeapi op 'submit'.
  $form['tnid'] = array(
    '#type' => 'hidden',
    '#value' => $form['#node']->tnid,
  );
}

/**
 * Callback invoked by translatable_get_form(), right after hook_form_alter().
 *
 * Prepopulates a node form with source content values and disables
 * untranslatable form fields.
 */
function translatable_node_alter_form($form_id, &$form) {
  if (!isset($form['type']) || $form['type']['#value'] .'_node_form' != $form_id) {
    return;
  }

  // Creating a new translation requires more work.
  if ($form['#node']->is_new) {
    global $user;

    // Load the source node.
    $source_node = node_load(arg(1));

    /*
    // Let 3rd party modules prepare certain form fields that need extra treatment.
    $_form = array();
    foreach (module_implements('translatable_prepare_node') as $module) {
      $function = $module .'_translatable_prepare_node';
      $result = $function($source_node);
      if (isset($result) && is_array($result)) {
        $_form = array_merge($_form, $result);
      }
    }
    */

    // Reset some values to defaults.
    $source_node->nid = NULL;
    $form['nid']['#value'] = NULL;
    $source_node->vid = NULL;
    $form['vid']['#value'] = NULL;
    $source_node->uid = $user->uid;
    $form['uid']['#value'] = $user->uid;
    $source_node->created = '';
    $form['created']['#value'] = '';
    if (isset($source_node->menu)) {
      $source_node->menu = NULL;
    }
    if (isset($source_node->path)) {
      $source_node->path = NULL;
    }
    // Replace the stored node in the form.
    $form['#node'] = $form['#parameters'][1] = $source_node;

    // Switch language
    translatable_set_adminlocale(arg(4), FALSE);
    
    // Generate a clean translation form and merge values from the source node form.
    $new_node = array(
      'uid' => $user->uid,
      'name' => $user->name,
      'type' => $source_node->type,
      'tnid' => $source_node->tnid,
      // @todo Get rid of arg(). Ideally, this would be already set in $form[#node]
      'language' => arg(4),
    );
    $new_form = translatable_retrieve_form($form_id, $new_node);
    $new_form['#is_new'] = TRUE;

    // Give modules a chance to assign #translatable after all form alterations
    // are done.  This is needed, because modules like CCK fieldgroup perform
    // excessive alterations.
    foreach (module_implements('translatable_form_alter') as $module) {
      $function = $module .'_translatable_form_alter';
      $function($form_id, $new_form);
    }

    translatable_filter_form($new_form, $form);
    $form = $new_form;

    // @doc Why?
    $form['#node']->translatable_node_any = 0;
  }
  else {
    // Editing a translation: load the source node.
    $parent_node = node_load($form['#node']->tnid);
    // Generate the parent editing form and use it to filter current values.
    $parent_form = translatable_retrieve_form($form_id, $parent_node);
    // Give modules a chance to assign #translatable after all form alterations
    // are done.  This is needed, because modules like CCK fieldgroup perform
    // excessive alterations.
    foreach (module_implements('translatable_form_alter') as $module) {
      $function = $module .'_translatable_form_alter';
      $function($form_id, $form);
    }
    translatable_filter_form($form, $parent_form);
  }
  
  // Set parent node id.
  $form['tnid'] = array(
    '#type' => 'hidden',
    '#value' => $form['#node']->tnid,
  );

  // Determine and set target language for new translations.
  if (!isset($form['#node']->language)) {
    $form['#node']->language = translatable_get_adminlocale();
  }
  $form['language'] = array(
    '#type' => 'hidden',
    '#default_value' => $form['#node']->language,
  );

  // Ensure that untranslatable fields are not altered.
  $form['#submit'] = array('translatable_filter_form_submit' => array($form)) + (array)$form['#submit'];
}

/**
 * Updates fields in translations of a node.
 *
 * If a node translation is saved, all untranslatable values have to be
 * copied from the source node into the translation node.
 *
 * If a source node is saved, all translations of this node have to be
 * updated to synchronize untranslatable values.
 *
 * @param &$node
 *    A node object to process.
 *
 * @todo Update to translatable_get_translatable_fields().
 */
function translatable_node_update_fields(&$node) {
  if ($node->nid == $node->tnid) {
    return;
  }
  // Retrieve a list of all node fields that have to be updated.
  $update_fields = module_invoke_all('translatable_fields', 'node_form');

  // Load field values from parent node.
  $source_node = node_load($node->tnid);

  // Update fields in translated node.
  foreach ($source_node as $field => $value) {
    if (isset($update_fields[$field])) {
      $node->$field = $value;
    }
  }
}

/**
 * Implementation of hook_link_alter().
 */
function translatable_link_alter(&$node, &$links) {
  if ($node->language != translatable_get_locale()) {
    foreach ($links as $id => $link) {
      if (isset($links[$id]['query'])) {
        $links[$id]['query'] = $links[$id]['query'] .'&locale='. $node->language;
      }
      else {
        $links[$id]['query'] = 'locale='. $node->language;
      }
    }
  }
}

/**
 * Intercepts all the calls for the page.
 */
function translatable_node_node_page() {
  $args = func_get_args();
  $op = array_shift($args);
  
  return translatable_page_translations($op, $args);
}

/**
 * Get the id of a node that is a translation of a given node in a given language.
 * 
 * @todo Make this function generic and move it to translatable.database.inc.
 *
 * @param $nid
 *   The parent node id.
 * @param $_locale
 *   The locale to search for.
 *
 * @return
 *   The corresponding translated node's nid or FALSE if no translation exists.
 */
function translatable_node_get_localizednid($nid, $locale) {
  $translatable_node = translatable_findbyid('node', $nid);
  if ($locale != $translatable_node['language']) {
    if ($tnid = $translatable_node['tnid']) {
      $translatable_node = translatable_find('node', 'tnid = '. (int)$tnid ." AND language = '". $locale ."'", FALSE);
      return $translatable_node['nid'];
    }
  }
  return FALSE;
}

function translatable_node_get_contentlocale($alias) {
  $path = drupal_get_normal_path($alias);
  $args = explode('/', $path);
  $language = '';
  if ($args[0] == 'node' && is_numeric($args[1])) {
    $translatable_node = translatable_findbyid('node', $args[1]);
    $language = $translatable_node['language'];
  }
  return $language;
}

function translatable_node_existscontentlocale($alias, $language) {
  $path = drupal_get_normal_path($alias);
  $args = explode('/', $path);
  $nid = '';
  if ($args[0] == 'node' && is_numeric($args[1])) {
    $translatable_node = translatable_findbyid('node', $args[1]);
    if ($tnid = $translatable_node['tnid']) {
      $translatable_node = translatable_find('node', 'tnid = '. (int)$tnid ." AND language = '". $language ."'", FALSE);
      $nid = $translatable_node['nid'];
    }
  }
  if ($nid) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
 * Temporary hard-coded #translatable field properties.
 */
function translatable_translatable_form_alter($form_id, &$form) {
  // Synchronize general node fields and properties.
  $form['title']['#translatable'] = TRUE;
  if (!empty($form['body_filter'])) {
    $form['body_filter']['#translatable'] = TRUE;
  }
  $form['options']['#translatable'] = TRUE;
  
  // Synchronize menu options for nodes; we default to Localizer (not i18n)
  // translations currently, so menu items are translated elsewhere.  Since
  // Drupal assumes the default locale as default language for all menu items,
  // menu items can only be added or edited in the default language.
  if (translatable_get_default_locale() != translatable_get_adminlocale()) {
    $form['menu']['#translatable'] = FALSE;
  }

  // Synchronize comment options from source.
  if (module_exists('comment')) {
    $form['comment_settings']['#translatable'] = TRUE;
  }
  
  // Disable taxonomy fields; we default to Localizer (not i18n) translations
  // currently, so taxonomies are translated elsewhere.
  if (module_exists('taxonomy')) {
    foreach (element_children($form['taxonomy']) as $key => $vocab) {
      $form['taxonomy'][$vocab]['#translatable'] = FALSE;
    }
  }
}

