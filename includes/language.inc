<?php

/**
 * @file
 * Language negotiation and determination functions.
 */

/**
 * Detect and set language.
 * 
 * Translatable tries to detect the language in this order:
 * - session variable
 * - user profile
 * - browser language
 * - default locale
 * 
 * @see locale_initialize(), translatable_change_uilocale()
 */
if (function_exists('i18n_get_lang')) {
  drupal_set_message(t('Translatable module must not be enabled concurrently with other multilingual modules. Please disable i18n and/or Localizer module(s) completely.'), 'error');
}
else {
  function i18n_get_lang() {
    global $user;
    
    if (isset($_SESSION['current_locale'])) {
      translatable_set_locale($_SESSION['current_locale']);
    }
    else if ($user->uid && isset($user->language)) {
      translatable_set_locale($user->language);
    }
    else if (variable_get('translatable_detect_browser', TRUE)) {
      translatable_set_locale(translatable_get_browser_lang());
    }
    else if (variable_get('translatable_switch_byhostname', FALSE)) {
      translatable_set_locale(translatable_localebyhostname($_SERVER['HTTP_HOST']));
    }
    else {
      translatable_set_locale(translatable_get_default_locale());
    }
    return translatable_get_locale();
  }
}

/**
 * Detect and switch the interface locale.
 *
 * @see translatable_menu()
 */
function translatable_change_uilocale() {
  $languages = translatable_available_languages();
  $locale = translatable_get_locale();
  
  // Redirect to localized front page.
  if (variable_get('translatable_frontpage_redirect', TRUE) && drupal_is_front_page()) {
    if (variable_get('translatable_switch_byhostname', FALSE)) {
      if ($locale != translatable_localebyhostname($_SERVER['HTTP_HOST'])) {
        $url  = 'http://';
        $url .= variable_get('translatable_switch_hostname_'. $locale, $locale .'.'. translatable_get_domain());
        $url .= base_path();
        $url .= translatable_get_destination();
        drupal_goto($url);
      }
    }
    
    if (translatable_get_defaultcontentlocale() != translatable_node_get_contentlocale($_GET['q']) && translatable_node_existscontentlocale($_GET['q'], translatable_get_defaultcontentlocale())) {
      $destination = translatable_get_destination();
      drupal_goto($destination);
    }
    return;
  }
  
  // Switch by hostname (reset locale).
  // Since this check has been added to i18n_get_lang(), this code is actually
  // executed if a user changes her locale in the user account settings only -
  // with the effect of overriding a user's account setting.
  if (variable_get('translatable_switch_byhostname', FALSE)) {
    $localebyhostname = translatable_localebyhostname($_SERVER['HTTP_HOST']);
    if ($locale != $localebyhostname) {
      translatable_set_locale($localebyhostname);
      drupal_goto(translatable_get_destination());
    }
    // End this function here.
    return;
  }
  
  // Switch by locale in query string.
  if (variable_get('translatable_switch_byparameter', TRUE) && isset($_GET['locale']) && $locale != $_GET['locale']) {
    translatable_set_locale($_GET['locale']);
    drupal_goto(translatable_get_destination());
  }
  
  // Switch by node locale.
  if (variable_get('translatable_switch_bynode', TRUE)) {
    if (arg(0) == 'node' && is_numeric(arg(1)) && arg(2)) {
      return;
    }
    $pathlocale = translatable_node_get_contentlocale($_GET['q']);
    if ($pathlocale && $locale != $pathlocale && isset($languages[$pathlocale]) && translatable_node_existscontentlocale($_GET['q'], $locale)) {
      translatable_set_locale($pathlocale);
      drupal_goto($_GET['q']);
    }
  }
  
  // Switch by locale prefix in path.
  if (variable_get('translatable_switch_bylocaleprefix', TRUE)) {
    $localeinurl = explode('/', $_REQUEST['q']);
    $localeinurl = $localeinurl[0];
    if ($locale != $localeinurl && isset($languages[$localeinurl])) {
      translatable_set_locale($localeinurl);
      drupal_goto($_GET['q']);
    }
  }
}

/**
 * Return the locale associated with an hostname
 *
 * @param $hostname
 *
 * @return
 * The locale associated
 */
function translatable_localebyhostname($hostname) {
  $localebyhostname = translatable_get_default_locale();
  foreach (translatable_available_languages() as $locale => $language) {
    $translatable_hostname = variable_get('translatable_switch_hostname_'. $locale, $locale .'.'. translatable_get_domain());
    if ($hostname == $translatable_hostname) {
      $localebyhostname = $locale;
      break;
    }
  }
  return $localebyhostname;
}

/**
 * Return a list of enabled languages.
 *
 * @param $getall
 *   (optional) Whether to retrieve all languages, including disabled.
 */
function translatable_available_languages($getall = FALSE) {
  static $languages;

  if (isset($languages) && !$getall) {
    return $languages;
  }
  $supported_languages = locale_supported_languages(FALSE, $getall);
  $supported_languages = $supported_languages['name'];

  if (!$getall) {
    $languages = $supported_languages;
  }
  return $getall ? $supported_languages : $languages;
}

/**
 * Get the language of the browser.
 */
function translatable_get_browser_lang() {
  $languages = translatable_available_languages();
  $accepted = split(',', $_SERVER['HTTP_ACCEPT_LANGUAGE']);
  if (!empty($accepted[0]) && isset($languages[$accepted[0]])) {
    return $accepted[0];
  }
  return translatable_get_default_locale();
}

/**
 * Get the current host.
 */
function translatable_get_host() {
  static $host;
  if (!isset($host)) {
    extract(parse_url($GLOBALS['base_url']));
  }
  return $host;
}

/**
 * Get the current domain.
 */
function translatable_get_domain() {
  static $domain;
  if (!isset($domain)) {
    $host_parts = array_reverse(explode('.', translatable_get_host()));
    $domain = $host_parts[1] .'.'. $host_parts[0];
  }
  return $domain;
}

/**
 * Check whether a given language code is valid.
 */
function translatable_validate_locale($locale) {
  if ($locale) {
    $languages = translatable_available_languages();
    return isset($languages[$locale]);
  }
  return FALSE;
}

/**
 * Return the configured default language of a Drupal site.
 */
function translatable_get_default_locale() {
  return key(translatable_available_languages());
}

/**
 * Get current language.
 */
function translatable_get_locale() {
  return $GLOBALS['locale'];
}

/**
 * Set new language.
 *
 * @param $locale
 *   The new locale.
 * @param bool $reload
 *   Whether to reload the current page after setting new locale.
 */
function translatable_set_locale($locale = '', $reload = FALSE) {
  if ($locale != $GLOBALS['locale'] && translatable_validate_locale($locale)) {
    $_SESSION['current_locale'] = $GLOBALS['locale'] = $locale;
    translatable_user_locale('adminlocale', $locale);
  }
  if ($reload) {
    drupal_goto();
  }
}

/**
 * Determine the current translation locale.
 * 
 * @param string $default
 *   A locale to return if translation locale is not yet set.
 */
function translatable_get_adminlocale($default = NULL) {
  if (empty($_SESSION['adminlocale'])) {
    $_SESSION['adminlocale'] = (!empty($default) && translatable_validate_locale($default)) ? $default : translatable_get_locale();
  }
  return $_SESSION['adminlocale'];
}

/**
 * Set the current translation locale.
 * 
 * @param string $locale
 *   A translation locale to switch to. If no locale is given, we switch to
 *   the current interface locale.
 * @param bool $reload
 *   Whether to reload the current page after setting new locale.
 */
function translatable_set_adminlocale($locale = NULL, $reload = TRUE) {
  if (!isset($locale)) {
    $locale = $GLOBALS['locale'];
  }
  $_SESSION['adminlocale'] = $locale;
  translatable_user_locale('locale', $locale);
  if ($reload) {
    drupal_goto();
  }
  return $locale;
}

/**
 * Attach language negotiation and determination settings to a given form.
 */
function translatable_language_negotiation_settings(&$form) {
  $form['localeswitch'] = array(
    '#type' => 'fieldset',
    '#title' => t('Language detection and switching options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['localeswitch']['translatable_detect_browser'] = array(
    '#type' => 'checkbox',
    '#title' => t('Detect through browser locale'),
    '#description' => t('Drupal tries to determine the language of a visitor by the language of the browser. In 99% of all cases the browser language is identical to the installation package language.'),
    '#default_value' => variable_get('translatable_detect_browser', TRUE),
  );
  $form['localeswitch']['translatable_frontpage_redirect'] = array(
    '#type' => 'checkbox',
    '#title' => t('Redirect front page to the localized version'),
    '#description' => t('If Drupal was able to determine the language of a visitor through one or all other listed options, the visitor will be redirected to the language-specific front page.'),
    '#default_value' => variable_get('translatable_frontpage_redirect', TRUE),
  );
  $form['localeswitch']['translatable_switch_byparameter'] = array(
    '#type' => 'checkbox',
    '#title' => t('Switch by query parameter %locale', array('%locale' => 'locale')),
    '#description' => t('The user interface language is switched if the URL includes the query parameter %locale, f.e. %example.', array('%locale' => 'locale', '%example' => url('node', 'locale=en'))),
    '#default_value' => variable_get('translatable_switch_byparameter', FALSE),
  );
  $form['localeswitch']['translatable_switch_bynode'] = array(
    '#type' => 'checkbox',
    '#title' => t('Switch by content language'),
    '#description' => t('The user interface language is switched each time a visitor accesses a content that is in another language than the active.'),
    '#default_value' => variable_get('translatable_switch_bynode', FALSE),
  );
  $form['localeswitch']['translatable_switch_bylocaleprefix'] = array(
    '#type' => 'checkbox',
    '#title' => t('Switch by language prefix in URL'),
    '#description' => t('If an URL begins with a language code, f.e. %url, the user interface language is switched accordingly.', array('%url' => 'en/node')),
    '#default_value' => variable_get('translatable_switch_bylocaleprefix', FALSE),
  );
  
  $form['localeswitch']['byhostname'] = array(
    '#type' => 'fieldset',
    '#title' => t('Switch by host name'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['localeswitch']['byhostname']['translatable_switch_byhostname'] = array(
    '#type' => 'checkbox',
    '#title' => t('Switch by host name'),
    '#default_value' => variable_get('translatable_switch_byhostname', FALSE),
    '#description' => t('If this option is enabled all other switching options are ignored.'),
  );
  foreach (translatable_available_languages(TRUE) as $locale => $language) {
    $form['localeswitch']['byhostname']['translatable_switch_hostname_'. $locale] = array(
      '#type' => 'textfield',
      '#default_value' => variable_get('translatable_switch_hostname_'. $locale, $locale .'.'. translatable_get_domain()),
      '#title' => t('@language hostname', array('@language' => $language)),
    );
  }
}

/**
 * Return a path without or with a specific language prefix.
 *
 * @param $path
 *   A path or alias to remove a language prefix.
 * @param $locale
 *   An optional language prefix to set.
 */
function translatable_get_path($path, $prefix = '') {
  $exploded_path = explode('/', $path);
  if (translatable_validate_locale($exploded_path[0])) {
    array_shift($exploded_path);
    $path = implode('/', $exploded_path);
  }
  if (translatable_validate_locale($prefix)) {
    $path = $prefix .'/'. $path;
  }
  return $path;
}

/**
 * Generates the path of a node for a given locale.
 *
 * @param string $path
 *   The internal path of a source node.
 * @param string $locale
 *   The locale to retrieve.
 *
 * @return
 *   The path to the corresponding translation.
 */
function translatable_get_localizedpath($path, $locale, $alias = TRUE) {
  $args = explode('/', $path);
  if ($args[0] == 'node' && is_numeric($args[1])) {
    $lnid = translatable_node_get_localizednid($args[1], $locale);
    if (!$lnid) {
      $lnid = $args[1];
    }
    $lpath = 'node/'. $lnid;
    for ($i = 2; $i < sizeof($args); $i++) {
      $lpath .= '/'. $args[$i];
    }
    return $alias ? drupal_get_path_alias($lpath) : $lpath;
  }
  return $path;
}

/**
 * Return the destination specific to the locale passed.
 *
 * Only used in theme_translatable_languages().
 *
 * @return
 *   The localized destination
 */
function translatable_get_destination($lang = NULL) {
  if (!isset($lang)) {
    $lang = translatable_get_defaultcontentlocale();
  }
  return translatable_get_localizedpath($_GET['q'], $lang);
}


/*****************************************************************************/
/**
 * @ingroup todo These functions are most likely obsolete. 16/02/2008 sun
 * @{
 */

function translatable_get_defaultcontentlocale($force = FALSE, $locale = '') {
  // Force a specific language if query parameter locale is set.
  if (isset($_GET['locale']) && translatable_validate_locale($_GET['locale'])) {
    return $_GET['locale'];
  }
  if (empty($locale) || !translatable_validate_locale($locale)) {
    $locale = translatable_get_locale();
  }
  return $locale;
}

/**
 * Get the current locales for the nodes.
 *
 * @return
 *   The current locales for nodes.
 */
function translatable_get_contentlocales($force = FALSE) {
  global $user;
  static $contentlocales = array();
  
  // Return translation target language from URI when translating a node.
  $_locale = arg(5);
  if (arg(3) == 'translation' && $_locale && translatable_validate_locale($_locale)) {
    return array($_locale => 1);
  }

  // Force a specific language if query parameter locale is set.
  if (isset($_GET['locale']) && translatable_validate_locale($_GET['locale'])) {
    return array($_GET['locale'] => 1);
  }

  if (!$contentlocales || $force) {
    $contentlocales = array();
    $contentlocales[translatable_get_locale()] = 1;
  }
  
  // Store content languages in user session for cache support.
  $_SESSION['translatable_contentlanguages'] = serialize($contentlocales);

  return $contentlocales;
}

/**
 * Get enabled locales.
 *
 * @param bool $sql
 *   Whether to return a string suitable for database queries (TRUE) or a pipe
 *   (|) delimited list in a string.
 * 
 * @return
 *   A comma-separated string containing currently enabled locales.
 */
function translatable_get_enabled_locales($sql = TRUE) {
  static $locales;
  
  if (isset($locales) && $sql) {
    return $locales;
  }
  
  $locales = array();
  foreach (translatable_get_contentlocales() as $key => $active) {
    if ($active) {
      $locales[] = $key;
    }
  }
  if ($sql) {
    $locales = "'". implode("', '", $locales) ."'";
  }
  else {
    $locales = implode('|', $locales);
  }
  return $locales;
}



