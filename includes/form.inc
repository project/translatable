<?php


/**
 * Retrieves a form from a builder function, passes it on for
 * processing, and renders the form or redirects to its destination
 * as appropriate. In multi-step form scenarios, it handles properly
 * processing the values using the previous step's form definition,
 * then rendering the requested step for display.
 *
 * @param $form_id
 *   The unique string identifying the desired form. If a function
 *   with that name exists, it is called to build the form array.
 *   Modules that need to generate the same form (or very similar forms)
 *   using different $form_ids can implement hook_forms(), which maps
 *   different $form_id values to the proper form building function. Examples
 *   may be found in node_forms(), search_forms(), and user_forms().
 * @param ...
 *   Any additional arguments needed by the form building function.
 * @return
 *   The rendered form.
 */
function translatable_get_form($form_id) {
  // In multi-step form scenarios, the incoming $_POST values are not
  // necessarily intended for the current form. We need to build
  // a copy of the previously built form for validation and processing,
  // then go on to the one that was requested if everything works.

  $form_build_id = md5(mt_rand());
  if (isset($_POST['form_build_id']) && isset($_SESSION['form'][$_POST['form_build_id']]['args']) && $_POST['form_id'] == $form_id) {
    // There's a previously stored multi-step form. We should handle
    // IT first.
    $stored = TRUE;
    $args = $_SESSION['form'][$_POST['form_build_id']]['args'];
    $form = call_user_func_array('drupal_retrieve_form', $args);
    $form['#build_id'] = $_POST['form_build_id'];
  }
  else {
    // We're coming in fresh; build things as they would be. If the
    // form's #multistep flag is set, store the build parameters so
    // the same form can be reconstituted for validation.
    $args = func_get_args();
    $form = call_user_func_array('drupal_retrieve_form', $args);
    if (isset($form['#multistep']) && $form['#multistep']) {
      // Clean up old multistep form session data.
      _drupal_clean_form_sessions();
      $_SESSION['form'][$form_build_id] = array('timestamp' => time(), 'args' => $args);
      $form['#build_id'] = $form_build_id;
    }
    $stored = FALSE;
  }

  // Process the form, submit it, and store any errors if necessary.
  translatable_process_form($args[0], $form);

  if ($stored && !form_get_errors()) {
    // If it's a stored form and there were no errors, we processed the
    // stored form successfully. Now we need to build the form that was
    // actually requested. We always pass in the current $_POST values
    // to the builder function, as values from one stage of a multistep
    // form can determine how subsequent steps are displayed.
    $args = func_get_args();
    $args[] = $_POST;
    $form = call_user_func_array('drupal_retrieve_form', $args);
    unset($_SESSION['form'][$_POST['form_build_id']]);
    if (isset($form['#multistep']) && $form['#multistep']) {
      $_SESSION['form'][$form_build_id] = array('timestamp' => time(), 'args' => $args);
      $form['#build_id'] = $form_build_id;
    }
    translatable_prepare_form($args[0], $form);
  }

  return drupal_render_form($args[0], $form);
}


/**
 * Retrieves the structured array that defines a given form.
 *
 * Generate the source editing forms array.  This differs from regular
 * drupal_retrieve_form() in that drupal_prepare_form() along with all
 * hook_form_alter() implementations is called, but the resulting form
 * structure has not been altered by Drupal's form_builder() yet.
 *
 * @param $form_id
 *   The unique string identifying the desired form. If a function
 *   with that name exists, it is called to build the form array.
 *   Modules that need to generate the same form (or very similar forms)
 *   using different $form_ids can implement hook_forms(), which maps
 *   different $form_id values to the proper form building function.
 * @param $node
 *   The source node to generate the editing form for.
 * @return
 *   An associative array containing the structure of the form.
 */
function translatable_retrieve_form($form_id) {
  $args = func_get_args();
  $form = call_user_func_array('drupal_retrieve_form', $args);
  translatable_prepare_form($form_id, $form, FALSE);
  return $form;
}

/**
 * This function is the heart of form API. The form gets built, validated and in
 * appropriate cases, submitted.
 *
 * @param $form_id
 *   The unique string identifying the current form.
 * @param $form
 *   An associative array containing the structure of the form.
 * @return
 *   The path to redirect the user to upon completion.
 */
function translatable_process_form($form_id, &$form) {
  global $form_values, $form_submitted, $user, $form_button_counter;
  static $saved_globals = array();
  // In some scenarios, this function can be called recursively. Pushing any pre-existing
  // $form_values and form submission data lets us start fresh without clobbering work done
  // in earlier recursive calls.
  array_push($saved_globals, array($form_values, $form_submitted, $form_button_counter));

  $form_values = array();
  $form_submitted = FALSE;
  $form_button_counter = array(0, 0);

  translatable_prepare_form($form_id, $form);
  if (($form['#programmed']) || (!empty($_POST) && (($_POST['form_id'] == $form_id)))) {
    drupal_validate_form($form_id, $form);
    // IE does not send a button value when there is only one submit button (and no non-submit buttons)
    // and you submit by pressing enter.
    // In that case we accept a submission without button values.
    if ((($form['#programmed']) || $form_submitted || (!$form_button_counter[0] && $form_button_counter[1])) && !form_get_errors()) {
      $redirect = drupal_submit_form($form_id, $form);
      if (!$form['#programmed']) {
        drupal_redirect_form($form, $redirect);
      }
    }
  }

  // We've finished calling functions that alter the global values, so we can
  // restore the ones that were there before this function was called.
  list($form_values, $form_submitted, $form_button_counter) = array_pop($saved_globals);
  return $redirect;
}

/**
 * Prepares a structured form array by adding required elements,
 * executing any hook_form_alter functions, and optionally inserting
 * a validation token to prevent tampering.
 *
 * @param $form_id
 *   A unique string identifying the form for validation, submission,
 *   theming, and hook_form_alter functions.
 * @param $form
 *   An associative array containing the structure of the form.
 * @param $translatable_build
 *   Whether to pass the generated forms array to form_builder() or return the
 *   raw data.
 */
function translatable_prepare_form($form_id, &$form, $translatable_build = TRUE) {
  global $user;

  $form['#type'] = 'form';

  if (!isset($form['#post'])) {
    $form['#post'] = $_POST;
    $form['#programmed'] = FALSE;
  }
  else {
    $form['#programmed'] = TRUE;
  }

  // In multi-step form scenarios, this id is used to identify
  // a unique instance of a particular form for retrieval.
  if (isset($form['#build_id'])) {
    $form['form_build_id'] = array(
      '#type' => 'hidden',
      '#value' => $form['#build_id'],
      '#id' => $form['#build_id'],
      '#name' => 'form_build_id',
    );
  }

  // If $base is set, it is used in place of $form_id when constructing validation,
  // submission, and theming functions. Useful for mapping many similar or duplicate
  // forms with different $form_ids to the same processing functions.
  if (isset($form['#base'])) {
    $base = $form['#base'];
  }

  // Add a token, based on either #token or form_id, to any form displayed to
  // authenticated users. This ensures that any submitted form was actually
  // requested previously by the user and protects against cross site request
  // forgeries.
  if (isset($form['#token'])) {
    if ($form['#token'] === FALSE || $user->uid == 0 || $form['#programmed']) {
      unset($form['#token']);
    }
    else {
      $form['form_token'] = array('#type' => 'token', '#default_value' => drupal_get_token($form['#token']));
    }
  }
  else if ($user->uid && !$form['#programmed']) {
    $form['#token'] = $form_id;
    $form['form_token'] = array(
      '#id' => form_clean_id('edit-'. $form_id .'-form-token'),
      '#type' => 'token',
      '#default_value' => drupal_get_token($form['#token']),
    );
  }


  if (isset($form_id)) {
    $form['form_id'] = array('#type' => 'hidden', '#value' => $form_id, '#id' => form_clean_id("edit-$form_id"));
  }
  if (!isset($form['#id'])) {
    $form['#id'] = form_clean_id($form_id);
  }

  $form += _element_info('form');

  if (!isset($form['#validate'])) {
    if (function_exists($form_id .'_validate')) {
      $form['#validate'] = array($form_id .'_validate' => array());
    }
    elseif (function_exists($base .'_validate')) {
      $form['#validate'] = array($base .'_validate' => array());
    }
  }

  if (!isset($form['#submit'])) {
    if (function_exists($form_id .'_submit')) {
      // We set submit here so that it can be altered.
      $form['#submit'] = array($form_id .'_submit' => array());
    }
    elseif (function_exists($base .'_submit')) {
      $form['#submit'] = array($base .'_submit' => array());
    }
  }

  foreach (module_implements('form_alter') as $module) {
    $function = $module .'_form_alter';
    $function($form_id, $form);
  }

  if ($translatable_build) {
    // Invoke our callback to merge in the translated values.
    translatable_node_alter_form($form_id, $form);
    $form = form_builder($form_id, $form);
  }
}

