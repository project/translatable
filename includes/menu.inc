<?php

/**
 * @file
 * Translation service for Drupal 5.x menu system.
 */

/**
 * Translates the menu tree and caches the result.
 */
function translatable_translate_menu() {
  // Don't translate when showing the menu admin page or when about to being
  // redirected there, as this would mess up the whole translations.
  if ($_GET['q'] == 'admin/build/menu' || (arg(0) == 'switchuilocale' && $_REQUEST['destination'] == 'admin/build/menu')) {
    return;
  }

  global $_menu, $user;

  // Current path without language prefix.
  $path_plain = translatable_get_path($_GET['q']);
  // Language codes of enabled languages.
  $locales = array_keys(translatable_available_languages());

  foreach ($_menu['items'] as $mid => $item) {
    if ($item['type'] & (MENU_CREATED_BY_ADMIN | MENU_MODIFIED_BY_ADMIN)) {
      $active = FALSE;

      // Check whether this path is a node path. If so, replace it with the path
      // to the translated node.
      $path = translatable_get_localizedpath($item['path'], translatable_get_defaultcontentlocale(), FALSE);
      if ($path != $item['path']) {
        $_menu['items'][$mid]['path'] = $path;
        $_menu['path index'][$path] = $mid;
      }

      // This item is active if it's path matches the current path, or
      // the current path with any enabled language code prefixed.
      if ($path == $_GET['q'] || $path == $path_plain) {
        $active = TRUE;
      }
      else {
        foreach ($locales as $locale) {
          if ($path == $locale .'/'. $path_plain) {
            $active = TRUE;
            break;
          }
        }
      }

      $translation = translatable_translate_menu_item($mid);
      if (isset($translation['title'])) {
        $_menu['items'][$mid]['title'] = $translation['title'];
      }
      if (isset($translation['description'])) {
        $_menu['items'][$mid]['description'] = $translation['description'];
      }
      
      if ($active) {
        $_menu['items'][$mid]['path'] = $_GET['q'];
        $locations = translatable_get_breadcrumb($mid);
        $front_page = drupal_get_normal_path(variable_get('site_frontpage', 'node'));
        $locpath = translatable_get_localizedpath($front_page, translatable_get_defaultcontentlocale());
        $locations[$locpath] = l(t('Home'), $locpath);
        $locations = array_reverse($locations);
        array_pop($locations);
        drupal_set_breadcrumb($locations);
      }
    }
  }
}

/**
 * Fetches the translation for a single menu item.
 * To reduce database load, all translations for the current locale are cached.
 *
 * @param $mid
 *   The menu item id to translate.
 */
function translatable_translate_menu_item($mid) {
  static $translations = array();

  if (!isset($translations[$mid])) {
    $items = translatable_find('translation', "object_name = 'menu' AND locale = '". translatable_get_locale() ."'");
    foreach ($items as $key => $item) {
      $translations[$item['object_key']][$item['object_field']] = $item['translation'];
    }
  }
  return $translations[$mid];
}

/**
 * Builds the breadcrumb path for a menu item.
 *
 * @param $mid
 *   The menu item id to build the breadcrumb path for.
 */
function translatable_get_breadcrumb($mid) {
  global $_menu;
  static $locations = array();

  if ($mid > 0) {
    $_menu['visible'][$mid]['type'] |= MENU_EXPANDED;
    if ($_menu['visible'][$mid]['type'] & MENU_VISIBLE_IN_BREADCRUMB) {
      $locpath = translatable_get_localizedpath($_menu['visible'][$mid]['path'], translatable_get_defaultcontentlocale());
      $locations[$locpath] = l($_menu['items'][$mid]['title'], $locpath);
    }
    translatable_get_breadcrumb($_menu['visible'][$mid]['pid']);
  }
  return $locations;
}

