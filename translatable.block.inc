<?php

/**
 * @file
 * Provides language and translation language blocks and user integration.
 * @todo Move to .module.
 */

/**
 * Implementation of hook_block().
 */
function translatable_block($op = 'list', $delta = 0, $edit = array()) {
  switch ($op) {
    case 'list':
      $blocks['ui'] = array('info' => t('Language'), 'region' => 'right', 'status' => 1);
      $blocks['translation']['info']   = t('Translation language');
      $blocks['translation']['region'] = 'right';
      $blocks['translation']['status'] = 1;
      $blocks['translation']['weight'] = -10;
      return $blocks;

    case 'configure':
      $form = array();
      if ($delta == 'ui' || $delta == 'translation') {
        $form['translatable_block_'. $delta .'_hidecurrentlocale'] = array(
          '#type' => 'checkbox',
          '#title' => t('Hide current language'),
          '#default_value' => variable_get('translatable_block_'. $delta .'_hidecurrentlocale', TRUE),
          '#description' => t('If this option is enabled, the current language is not displayed in the language switching block.'),
        );
        $flag_options = array(
          'flag' => t('Show flags'),
          'name' => t('Show language names'),
          'flag-name' => t('Show flags and language names'),
        );
        $form['translatable_block_'. $delta .'_style'] = array(
          '#type' => 'select',
          '#title' => t('Display style'),
          '#options' => $flag_options,
          '#default_value' => variable_get('translatable_block_'. $delta .'_style', 'flag-name'),
        );
        $form['translatable_block_'. $delta .'_flagspath'] = array(
          '#type' => 'textfield',
          '#title' => t('Flag icons path'),
          '#default_value' => variable_get('translatable_block_'. $delta .'_flagspath', drupal_get_path('module', 'translatable') .'/flags/*.png'),
          '#size' => 50,
          '#maxlength' => 180,
          '#description' => t("Path to flag images, relative to the Drupal installation directory. '*' will be replaced with the ISO language code."),
        );
      }
      return $form;
      
    case 'save':
      if ($delta == 'ui' || $delta == 'translation') {
        variable_set('translatable_block_'. $delta .'_hidecurrentlocale', $edit['translatable_block_'. $delta .'_hidecurrentlocale']);
        variable_set('translatable_block_'. $delta .'_style', $edit['translatable_block_'. $delta .'_style']);
        variable_set('translatable_block_'. $delta .'_flagspath', $edit['translatable_block_'. $delta .'_flagspath']);
      }
      return;
      
    case 'view':
      if ($delta == 'ui') {
        $block['subject'] = t('Language');
        $languages = theme('translatable_languages', 'ui');
        $block['content'] = theme('item_list', theme('translatable_language_link', $languages));
      }
      else if ($delta == 'translation') {
        $block['subject'] = t('Translation language');
        $languages = translatable_adminlocale_formselect(NULL, NULL);
        if (!empty($languages)) {
          $block['content'] = theme('item_list', theme('translatable_language_link', $languages));
        }
      }
      return $block;
  }
}

/**
 * Create the links for the switching language block
 *
 * @param string $block
 *   The Translatable block name, can be 'ui', 'content' or 'translation'.
 * @param array $form
 *   The form on which the languages should be based on.
 *
 * @return
 *   A HTML list of links of available languages.
 */
function theme_translatable_languages($block = 'ui', $form = NULL) {
  global $user;
  static $tset, $nodes, $translation_form;

  drupal_add_css(drupal_get_path('module', 'translatable') .'/translatable.css');
  $style = variable_get('translatable_block_'. $block .'_style', 'flag-name');

  // Cache the form for subsequent requests.
  if (!empty($form)) {
    $translation_form = $form;
  }

  // When editing a node, fetch existent translations.
  if (!isset($tset) && arg(0) == 'node' && arg(2) == 'edit') {
    $tset = translatable_findbyid('node', arg(1));
    if ($tset) {
      $nodes = translatable_find('node', "tnid = ". $tset['tnid']);
    }
  }

  foreach (translatable_available_languages() as $lang => $langname) {
    // Skip active language if enabled.
    if (variable_get('translatable_block_'. $block .'_hidecurrentlocale', FALSE) && $lang == translatable_get_locale()) {
      continue;
    }

    $langname = translatable_t($langname, 0, $lang);

    switch ($style) {
      case 'flag':
        $title = theme('translatable_flag', $lang, $langname, $block);
        break;
        
      case 'name':
        $title = $langname;
        break;
        
      case 'flag-name':
        $title = theme('translatable_flag', $lang, $langname, $block) . $langname;
        break;
    }
    $attribs = array('title' => $langname, 'class' => 'language');
    $absolute = FALSE;

    // Determine redirect destination for links in translation language block
    // or both blocks, if user does not prefer separate languages.
    // @todo Only for enabled content types!
    $destination = NULL;
    if (arg(0) == 'node' && ($block == 'translation' || !$user->switch_adminlocale)) {
      if (arg(1) == 'add') {
        // When creating a new node, we simply switch the language and reload.
        $destination = $_GET['q'];
      }
      else if (isset($nodes)) {
        // When editing a node, we need to search all translations for the
        // current parent id to build corresponding links.
        $node = FALSE;
        foreach ($nodes as $tnode) {
          if ($tnode['language'] == $lang) {
            $node = $tnode;
            break;
          }
        }
        if ($node) {
          // A translation exists; redirect to regular node edit page.
          $destination = 'node/'. $node['nid'] .'/edit';
        }
        else {
          // No translation exists; redirect to our callback.
          $destination = 'node/'. $tset['tnid'] .'/edit/translation/'. $lang;
        }
      }
    }

    switch ($block) {
      case 'ui':
        if (!isset($destination)) {
          $destination = translatable_get_destination($lang);
          if ($destination == '') {
            $destination = variable_get('site_frontpage', 'node');
          }
          if (strpos($destination, 'destination=') !== FALSE) {
            $destination = $_GET['q'];
          }
        }
        if (variable_get('translatable_switch_byhostname', FALSE)) {
          $absolute = TRUE;
          $host = variable_get('translatable_switch_hostname_'. $lang, $lang .'.'. translatable_get_domain());
          $url   = 'http://'. $host . $GLOBALS['base_path'];
          if (variable_get('clean_url', 0)) {
            $url .= $destination;
            $query = NULL;
          }
          else {
            $query = 'q='. $destination;
          }
        }
        else {
          $url   = 'switchuilocale/'. $lang;
          $query = 'destination='. drupal_urlencode($destination);
        }
        $attribs['class'] .= ($lang == translatable_get_locale() ? ' active' : '');
        break;
        
      case 'translation':
        if (isset($destination)) {
          $attribs['class'] .= ($lang == $form['language']['#default_value'] || $lang == translatable_get_adminlocale() ? ' active' : '');
        }
        else {
          // Simple translation form.
          $destination = $_GET['q'];
          $attribs['class'] .= ($lang == translatable_get_adminlocale() ? ' active' : '');
        }
        if (isset($translation_form)) {
          $message = t('Please wait...');
          $attribs['onclick'] = "$('#". str_replace('_', '-', $translation_form['#id']) ."').block('$message');";
        }
        $url   = 'switchadminlocale/'. $lang;
        $query = 'destination='. drupal_urlencode($destination);
        break;
    }
    $links[$lang] = array('title' => $title, 'path' => $url, 'attribs' => $attribs, 'query' => $query, 'fragment' => NULL, 'absolute' => $absolute, 'html' => TRUE);
  }
  return $links;
}

/**
 * Render the links for the language switcher block.
 *
 * This stub function is needed to allow other modules to alter the links
 * in front of output.
 *
 * @param $languages
 *   An array of languages, keyed by language code, as returned by
 *   theme_translatable_languages().
 *
 * @see theme_translatable_languages()
 */
function theme_translatable_language_link($languages = array()) {
  foreach ($languages as $lang => $link) {
    $languages[$lang] = l($link['title'], $link['path'], $link['attribs'], $link['query'], $link['fragment'], $link['absolute'], $link['html']);
  }
  return $languages;
}

/**
 * Switch the locale of an user, if enabled.
 *
 * @param $type
 *   Which locale to switch; 'locale' or 'adminlocale'.
 * @param $locale
 *   A language to switch to; already validated.
 */
function translatable_user_locale($type, $locale) {
  global $user;
  
  // Only switch if the user does not prefer separate locales.
  if (!$user->switch_adminlocale) {
    $function = 'translatable_set_'. $type;
    $function($locale, FALSE);
  }
}

/**
 * Implementation of hook_admin_menu().
 */
function translatable_admin_menu(&$admin_menu, $may_cache) {
  if (!user_access('access translatable')) {
    return;
  }
  if (!$may_cache) {
    // Display current translation language first and prefix it with a
    // meaningful title.
    $languages = translatable_adminlocale_formselect(NULL, NULL);
    $adminlocale = translatable_get_adminlocale();
    $languages[$adminlocale]['title'] = t('Language') .': '. $languages[$adminlocale]['title'];

    $mid_admin = $admin_menu['index']['admin'];
    $mid_language = admin_menu_add_item($admin_menu, $mid_admin, array('title' => $languages[$adminlocale]['title'], 'description' => $languages[$adminlocale]['attribs']['title'], 'path' => $languages[$adminlocale]['path'], 'query' => $languages[$adminlocale]['query'], 'weight' => 10));
    unset($languages[$adminlocale]);

    foreach ($languages as $link) {
      admin_menu_add_item($admin_menu, $mid_language, array('title' => $link['title'], 'description' => $link['attribs']['title'], 'path' => $link['path'], 'query' => $link['query'], 'attributes' => $link['attribs']));
    }
  }
}

