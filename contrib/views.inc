<?php

function translatable_views_tables() {
  $tables['node_translatable'] = array(
    'name' => 'node_translatable',
    'join' => array(
      'left' => array(
        'table' => 'node',
        'field' => 'nid'
      ),
      'right' => array(
        'field' => 'nid'
      ),
    ),
    'fields' => array(
      'language' => array(
        'name' => 'Node: Language',
        'handler' => 'views_handler_translatable_contentlocales',
        'option' => array(
          '#type' => 'select',
          '#options' => array(
            'text_without_flag' => t('Text without flag'),
            'text_with_flag' => t('Text with flag'),
            'only_flag' => t('Only flag')
          ),
        ),
        'sortable' => true,
      ),
    ),
    'sorts' => array(
      'language' => array('name' => 'Node: Language')
    ),
    'filters' => array(
      'translatable_languages' => array(
        'name' => 'Translatable: Node languages',
        'operator' => array('is' => t('is')),
        'list' => 'views_handler_translatable_filter_contentlocales_list',
        'handler' => 'views_handler_translatable_filter_contentlocales',
      ),
      'translatable_support' => array(
        'name' => 'Translatable: Generic support',
        'operator' => array('is' => t('is')),
        'handler' => 'views_handler_translatable_generic_support',
        'list' => array(1 => t('Enabled'), 0 => t('Disabled')),
        'list-type' => 'select',
      )
    ),
  );
  return $tables;
}

function views_handler_translatable_contentlocales($fieldinfo, $fielddata, $value, $data) {
  static $languages;
  
  if (!$languages) {
    $languages = translatable_available_locales();
  }
  $lang      = $value;
  $langname  = strtr('@language', array('@language' => $languages[$value]));
  if ($fielddata['options'] == 'text_with_flag') {
    $flag      = theme('translatable_flag', $lang, $langname, 'content');
    $separator = variable_get('translatable_block_content_flagseparator', ' ');
    $separator = ($langname != '' && $flag != '') ? $separator : '';
    return $flag . $separator . $langname;
  }
  else if ($fielddata['options'] == 'only_flag') {
    $flag = theme('translatable_flag', $lang, $langname, 'content');
    return $flag;
  }
  else {
    return t('@language', array('@language' => $languages[$value]));
  }
}

function views_handler_translatable_filter_contentlocales_list() {
  static $languages;
  if (!isset($languages)) {
    $languages = translatable_available_locales();
  }
  return $languages;
}

function views_handler_translatable_filter_contentlocales($op, $filter, $filterinfo, &$query) {
  if ($filter['value']) {
    $values = array();
    if (is_array($filter['value'])) {
      $values = $filter['value'];
    }
    else if (is_string($filter['value'])) {
      $values = explode(',', $filter['value']);
    }
    foreach ($values as $key => $value) {
      $values[$key] = "'". $value ."'";
    }
    $query->ensure_table('node_translatable');
    $query->add_where('node_translatable.language IN ('. implode(',', $values) .') OR node_translatable.any = 1');
  }
}

function views_handler_translatable_generic_support($op, $filter, $filterinfo, &$query) {
  if ($filter['value'] == 1) {
    $query->ensure_table('node_translatable');
    $query->add_where('node_translatable.language IN ('. translatable_get_enabled_locales() .') OR node_translatable.any = 1');
  }
}

