<?php

/**
 * Implementation of hook_translatable_prepare_node().
 * 
 * Category module builds the tree of available categories upon node_form()
 * which is invoked by Translatable to clone the source node. Since the locale is
 * set to the source language at this time, the category selection includes
 * categories of that language only. The form has to be rebuilt for the new
 * language.
 * 
 * We need to return a new form array to overwrite the old entries. These will
 * be merged in translatable_node_form_alter() after the form for the modified
 * source_node has been retrieved.
 * 
 * Only the first line is different.
 * @see category_form()
 */
function category_translatable_prepare_node(&$node) {
  $node->cnid = translatable_node_get_localizednid($node->cnid, translatable_get_adminlocale($node->language));

  $parent_cont = $node->cnid;
  $is_cat = $node->type == 'category_cat';

  if (is_numeric($parent_cont)) {
    $category = category_get_category($parent_cont);
    $container = NULL;

    if ($category->cnid) {
      $container = category_get_container($category->cnid);
    }
    else {
      $container = category_get_container($parent_cont);
    }

    $container->cnid = $container->cid;
    unset($container->cid);
    unset($container->title);

    if ($is_cat) {
      foreach ($container as $key => $value) {
        $node->$key = $value;
      }
    }

    $node->parent_cont = $parent_cont;
  }

  $form = category_get_form($is_cat, $node);
  return $form;
}

/**
 * Limits category selection forms in node/edit to the node's language.
 */
function category_translatable_rewrite_sql($query, $primary_table, $primary_field) {
  return ($primary_table == 'cn' && $primary_field == 'cid');
}

/**
 * Implementation of hook_translatable_fields().
 */
function category_translatable_fields($form_id) {
  if ($form_id == 'node_form') {
    return array(
      'nodes' => 1,
      'hierarchy' => 1,
      'has_relations' => 1,
      'has_synonyms' => 1,
      'tags' => 1,
      'multiple' => 1,
      'required' => 1,
      'hidden_cont' => 1,
      'cnid' => 1,
      'parents' => 1,
      'weight' => 1,
      'depth' => 1,
      'children_allowed_parents' => 1,
    );
  }
}

/**
 * Implementation of hook_translatable_fields().
 */
function category_display_translatable_fields($form_id) {
  if ($form_id == 'node_form') {
    return array(
      'navlinks' => 1,
    );
  }
}

