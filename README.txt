
-- SUMMARY --

Translatable module is a different approach on Internationalization and
Localization.  It is based on the fact that any translatable content in Drupal
is input via forms that have been built with the Forms API.

Any form item can have the property #translatable.  If that property is set to
FALSE, the contents of the form item cannot be translated.  If it is not set,
Translatable module assumes that it can be translated.  If it is set to TRUE,
Translatable module automatically retrieves and displays the source contents
to translate the form item's content.

For this purpose, it adds a layer between Forms API and database storage.
Translatable uses two methods for data abstraction. These are:

- Generic translation objects, like blocks, menu items or taxonomy terms.
- Synchronized translations, like nodes.

Translation objects are stored as simple strings in the database.  Source
strings are currently replaced with localized strings using separate
translation queries upon view.  Any contributed module can make use of this
translation method.  Retrieving translated strings using db_rewrite_sql() is 
basically possible, however, not really supported by Drupal core - thus, still
CNW.

Synchronized translations are relations between two objects that are in a
translation set.  For example, node/18 might be the German translation of
an English source content in node/16.  Synchronized translations are suitable
for all contents that need to attach a language to build a "translation set".
Because of that, nodes are the only contents that are synchronized currently.
Translatable already implements the same node properties that can be found in
Drupal 6.

Translatable provides support for most used Drupal core objects, i.e. nodes,
blocks, menu items, and taxonomy vocabularies and terms.  Due to its design,
support for CCK-based nodes is built-in.

By speaking of Drupal 6:  Though this module should work as claimed, do not
expect any support for Drupal 5.  If you want to give it a try, install it on
a test site first.  Top priority is to port Translatable to Drupal 6 and to
create patches for Drupal 7 core as soon as possible.

By speaking of patches:  Like Localizer, this module requires you to apply a
patch against Drupal core to allow translation of blocks and taxonomies.
Patches for all available official releases of Drupal core are included.

Translatable is partially based on the following modules:
- Localizer (Roberto Gerola)
- Internationalization (Jose A. Reyero, <http://drupal.org/project/i18n>)
- Translation (Axel)
- Switchlang (Heine Deelstra)
- Translate (Rob Ellis)


-- REQUIREMENTS --

* Locale (Drupal core).


-- INSTALLATION --

* Check your default locale in admin/settings/locale in front of installing this
  module.  Existing nodes will be flagged with this language and there is no way
  to alter the language of existing nodes currently.

* Install as usual, see http://drupal.org/node/70151 for further information.

* Enable the module in administer >> Modules.

* Enable the blocks by visiting administer >> Site building >> Blocks.

* Apply the included patch for Taxonomy and Block module in Drupal core from
  the patches/ directory.  For further information, see README.txt in that
  directory.


-- CONFIGURATION --

* Configure language negotiation options and supported content types in
  administer >> Settings >> Translatable


-- USAGE --

* On edit forms, click on your desired translation language in the "Translation
  language" block.  After the page reloaded, you can submit your translation.
  Please bear in mind, that this block is only displayed if you configured it
  correctly on administer >> Site building >> Blocks.

* When viewing a node, click on the "Translations" tab to manage assigned
  translations of a node.  You can add existing nodes to a translation set and
  remove a node from a translation set there.


-- CUSTOMIZATION --

* To enable further forms (form ids) or form items for translation, have a look
  at

  - translatable_alter_core_forms in translatable.object.inc
  - translatable_alter_node_form() in translatable.node.inc

* Hooks for doing this are in the planning stage.  Wanna help out?


-- TROUBLESHOOTING --

* If your translations do not show up, ensure that you have properly applied
  the included patches for Drupal core.


-- FAQ --

Q: Creating or editing menu items in a node form is not possible, why?

A: Drupal assigns custom menu items to the configured default locale
   (see admin/settings/locale).  If you would create or edit such an object in
   a different language, Drupal would still assign it to the default locale.
   Because of that, those objects can only created and edited in the configured
   default locale of Drupal.

Q: I posted a bug report, feature request, or support request in the module's
   issue queue.  Why has it been closed?

A: As long as there is no volunteer who maintains and contributes to the issue
   queue, there is no user support for Translatable.  Developers only.  If you
   like this project, consider applying as a volunteer.


-- CONTACT --

Current maintainers:
* Daniel F. Kudwien (sun) - dev@unleashedmind.com
* Stefan M. Kudwien (smk-ka) - dev@unleashedmind.com

Credits:
* Roberto Gerola - roberto.gerola@speedtech.it
* Jose A Reyero
* Anton Sidashin (Troy)

This project has been sponsored by:
* UNLEASHED MIND
  Specialized in consulting and planning of Drupal powered sites, UNLEASHED
  MIND offers installation, development, theming, customization, and hosting
  to get you started. Visit http://www.unleashedmind.com for more information.


